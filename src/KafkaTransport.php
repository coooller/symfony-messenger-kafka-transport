<?php

declare(strict_types=1);

namespace CoooLLer\KafkaTransport;

use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use RdKafka;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Stamp\DelayStamp;
use Symfony\Component\Messenger\Stamp\RedeliveryStamp;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface as MessengerSerializer;
use Symfony\Component\Messenger\Transport\TransportInterface;
use CoooLLer\KafkaTransport\Event\SendingMessageToKafkaEvent;
use CoooLLer\KafkaTransport\Exception\KafkaConsumeException;
use CoooLLer\KafkaTransport\Exception\KafkaPublishException;
use CoooLLer\KafkaTransport\Stamp\AckStamp;
use CoooLLer\KafkaTransport\Stamp\KeyStamp;
use CoooLLer\KafkaTransport\Stamp\TopicStamp;

class KafkaTransport implements TransportInterface
{
    /** @var MessengerSerializer */
    private $serializer;
    /** @var EventDispatcherInterface */
    private $eventDispatcher;
    /** @var KafkaTransportConfiguration */
    private $configuration;
    /** @var Envelope | null */
    private $redeliveredEnvelope;
    /** @var RdKafka\KafkaConsumer | null */
    private $consumer;
    /** @var LoggerInterface|null */
    private $logger;

    /**
     * @param MessengerSerializer $serializer
     * @param EventDispatcherInterface $eventDispatcher
     * @param KafkaTransportConfiguration $configuration
     * @param LoggerInterface|null $logger
     */
    public function __construct(
        MessengerSerializer         $serializer,
        EventDispatcherInterface    $eventDispatcher,
        KafkaTransportConfiguration $configuration,
        LoggerInterface             $logger = null
    ) {
        $this->serializer = $serializer;
        $this->eventDispatcher = $eventDispatcher;
        $this->configuration = $configuration;
        $this->logger = $logger;

        if (null !== $this->logger) {
            $this->logger->debug('Transport created');
        }
    }

    public function __destruct()
    {
        $this->close();
    }

    /**
     * @param Envelope $envelope
     *
     * @return Envelope
     * @throws KafkaPublishException
     */
    public function send(Envelope $envelope): Envelope
    {
        if ($delay = $envelope->last(DelayStamp::class)) {
            sleep($delay->getDelay() / 1000);
        }

        if ($envelope->last(RedeliveryStamp::class)) {
            $this->redeliveredEnvelope = $envelope;

            return $envelope;
        }

        $envelope = $this->dispatchSendingEvent($envelope);
        $producer = new RdKafka\Producer($this->configuration->getRdCafkaConf());
        $encodedEnvelope = $this->serializer->encode($envelope);
        $topic = $producer->newTopic($this->configuration->getTopicName());

        $key = ($keyStamp = $envelope->last(KeyStamp::class))
            ? $keyStamp->getKey()
            : null;

        $headers = array_merge($this->configuration->getHeaders(), $encodedEnvelope['headers'] ?? []);
        $topic->producev(RD_KAFKA_PARTITION_UA, 0, $encodedEnvelope['body'], $key, $headers);

        $producer->poll(0);
        $result = $producer->flush($this->configuration->getWriteTimeoutMs());
        if (RD_KAFKA_RESP_ERR_NO_ERROR !== $result) {
            throw new KafkaPublishException(rd_kafka_err2str($result), $result);
        }

        return $envelope;
    }

    /**
     * @return iterable
     * @throws KafkaConsumeException
     * @throws RdKafka\Exception
     */
    public function get(): iterable
    {
        $envelope = $this->getRedeliveredEnvelope() ?: $this->consume();

        if (null === $envelope) {
            return;
        }

        yield $envelope;
    }

    /**
     * @param Envelope $envelope
     */
    public function reject(Envelope $envelope): void
    {
        $envelope->withoutStampsOfType(AckStamp::class);
    }

    public function ack(Envelope $envelope): void
    {
        $envelope->last(AckStamp::class)();
    }

    /**
     * @return Envelope|null
     */
    private function getRedeliveredEnvelope(): ?Envelope
    {
        $envelope = $this->redeliveredEnvelope;
        $this->redeliveredEnvelope = null;

        return $envelope;
    }

    /**
     * @return Envelope|null
     * @throws KafkaConsumeException
     * @throws RdKafka\Exception
     */
    private function consume(): ?Envelope
    {
        if (null === $this->consumer) {
            $this->consumer = new RdKafka\KafkaConsumer($this->configuration->getRdCafkaConf());
            if (null !== $this->logger) {
                $this->logger->debug('Consumer created');
            }
        }

        if (empty($this->consumer->getSubscription())) {
            $this->consumer->subscribe([$this->configuration->getTopicName()]);
            if (null !== $this->logger) {
                $this->logger->debug('Consumer subscribed on topic ' . $this->configuration->getTopicName());
            }
        }

        $message = $this->consumer->consume($this->configuration->getReadTimeoutMs());
        if (RD_KAFKA_RESP_ERR__TIMED_OUT === $message->err) {
            if (null !== $this->logger) {
                $this->logger->debug('Poll message timeout');
            }

            foreach ($this->consumer->getAssignment() as $topicPartition) {
                if (method_exists($topicPartition, 'getErr') && RD_KAFKA_RESP_ERR_NO_ERROR !== $topicPartition->getErr()) {
                    throw new KafkaConsumeException(sprintf(
                        'Topic %s; partition %s; err %s',
                        $topicPartition->getTopic(),
                        $topicPartition->getPartition(),
                        rd_kafka_err2str($topicPartition->getErr())
                    ));
                }
            }

            return null;
        }

        if (RD_KAFKA_RESP_ERR_NO_ERROR !== $message->err) {
            throw new KafkaConsumeException($message->errstr(), $message->err);
        }

        $envelope = $this->serializer
            ->decode([
                'body'    => $message->payload,
                'key'     => $message->key ?? null,
                'headers' => array_merge($message->headers ?? [], $this->configuration->getHeaders()),
            ])
            ->with(new AckStamp(function () use ($message) {
                $this->consumer->commit($message);
            }))
            ->with(new KeyStamp($message->key ?? ''));

        return $envelope;
    }

    private function dispatchSendingEvent(Envelope $envelope): Envelope
    {
        if ($envelope->last(TopicStamp::class) === null) {
            $envelope = $envelope->with(new TopicStamp($this->configuration->getTopicName()));
        }

        $event = new SendingMessageToKafkaEvent($envelope);
        $this->eventDispatcher->dispatch($event);

        return $event->getEnvelope();
    }

    public function close()
    {
        if (null !== $this->consumer) {
            $consumer = $this->consumer;
            $this->consumer = null;
            $consumer->close();

            if (null !== $this->logger) {
                $this->logger->debug('Kafka connection closed');
            }
        }
    }
}
