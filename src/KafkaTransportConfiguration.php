<?php

namespace CoooLLer\KafkaTransport;

use RdKafka;

class KafkaTransportConfiguration
{
    /** @var string */
    private $topicName;
    /** @var RdKafka\Conf */
    private $rdCafkaConf;
    /** @var array */
    private $headers = [];
    /** @var int */
    private $writeTimeoutMs = 10000;
    /** @var int */
    private $readTimeoutMs = 10000;

    /**
     * @return string
     */
    public function getTopicName(): string
    {
        return $this->topicName;
    }

    /**
     * @param string $topicName
     * @return KafkaTransportConfiguration
     */
    public function setTopicName(string $topicName): KafkaTransportConfiguration
    {
        $this->topicName = $topicName;

        return $this;
    }

    /**
     * @return RdKafka\Conf
     */
    public function getRdCafkaConf(): RdKafka\Conf
    {
        return $this->rdCafkaConf;
    }

    /**
     * @param RdKafka\Conf $rdCafkaConf
     * @return KafkaTransportConfiguration
     */
    public function setRdCafkaConf(RdKafka\Conf $rdCafkaConf): KafkaTransportConfiguration
    {
        $this->rdCafkaConf = $rdCafkaConf;

        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param array $headers
     * @return KafkaTransportConfiguration
     */
    public function setHeaders(array $headers): KafkaTransportConfiguration
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * @return int
     */
    public function getWriteTimeoutMs(): int
    {
        return $this->writeTimeoutMs;
    }

    /**
     * @param int $writeTimeoutMs
     * @return KafkaTransportConfiguration
     */
    public function setWriteTimeoutMs(int $writeTimeoutMs): KafkaTransportConfiguration
    {
        $this->writeTimeoutMs = $writeTimeoutMs;

        return $this;
    }

    /**
     * @return int
     */
    public function getReadTimeoutMs(): int
    {
        return $this->readTimeoutMs;
    }

    /**
     * @param int $readTimeoutMs
     * @return KafkaTransportConfiguration
     */
    public function setReadTimeoutMs(int $readTimeoutMs): KafkaTransportConfiguration
    {
        $this->readTimeoutMs = $readTimeoutMs;

        return $this;
    }
}