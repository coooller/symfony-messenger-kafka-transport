<?php

declare(strict_types=1);

namespace CoooLLer\KafkaTransport\Stamp;

use Symfony\Component\Messenger\Stamp\StampInterface;

class AckStamp implements StampInterface
{
    private $ack;

    public function __construct(callable $ack = null)
    {
        $this->ack = $ack;
    }

    public function __invoke()
    {
        if ($this->ack !== null) {
            call_user_func($this->ack);
        }
    }
}
