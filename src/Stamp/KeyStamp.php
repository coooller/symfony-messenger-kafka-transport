<?php

namespace CoooLLer\KafkaTransport\Stamp;

use Symfony\Component\Messenger\Stamp\StampInterface;

class KeyStamp implements StampInterface
{
    /**
     * @var string
     */
    private $key;

    /**
     * KeyStamp constructor.
     * @param $key
     */
    public function __construct(string $key)
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }
}
