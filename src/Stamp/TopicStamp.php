<?php

namespace CoooLLer\KafkaTransport\Stamp;

use Symfony\Component\Messenger\Stamp\StampInterface;

class TopicStamp implements StampInterface
{
    /** @var string */
    private $topicName;

    /**
     * @param string $topicName
     */
    public function __construct(string $topicName)
    {
        $this->topicName = $topicName;
    }

    /**
     * @return string
     */
    public function getTopicName(): string
    {
        return $this->topicName;
    }
}