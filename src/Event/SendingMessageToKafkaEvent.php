<?php

namespace CoooLLer\KafkaTransport\Event;

use Symfony\Component\Messenger\Envelope;

class SendingMessageToKafkaEvent
{
    private $envelope;

    public function __construct(Envelope $envelope)
    {
        $this->envelope = $envelope;
    }

    public function getEnvelope(): Envelope
    {
        return $this->envelope;
    }

    public function setEnvelope(Envelope $envelope): self
    {
        $this->envelope = $envelope;

        return $this;
    }
}