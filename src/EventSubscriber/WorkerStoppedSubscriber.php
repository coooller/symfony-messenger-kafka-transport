<?php

namespace CoooLLer\KafkaTransport\EventSubscriber;

use Symfony\Component\DependencyInjection\ServiceLocator;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Messenger\Event\WorkerStoppedEvent;
use CoooLLer\KafkaTransport\KafkaTransport;

class WorkerStoppedSubscriber implements EventSubscriberInterface
{
    /** @var ServiceLocator */
    private $serviceLocator;

    public function __construct(ServiceLocator $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function closeConnection(WorkerStoppedEvent $event)
    {
        foreach ($event->getWorker()->getMetadata()->getTransportNames() as $transportName) {
            $transport = $this->serviceLocator->get($transportName);
            if ($transport instanceof KafkaTransport) {
                $transport->close();
            }
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            WorkerStoppedEvent::class => 'closeConnection',
        ];
    }
}
