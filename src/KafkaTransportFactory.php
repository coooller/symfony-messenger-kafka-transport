<?php

namespace CoooLLer\KafkaTransport;

use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use RdKafka;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
use Symfony\Component\Messenger\Transport\TransportFactoryInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;
use CoooLLer\KafkaTransport\Exception\KafkaConsumeException;

class KafkaTransportFactory implements TransportFactoryInterface
{
    public const PROTOCOL = 'kafka';
    private const LOG_LEVEL_MAP = [
        LOG_DEBUG   => LogLevel::DEBUG,
        LOG_INFO    => LogLevel::INFO,
        LOG_NOTICE  => LogLevel::NOTICE,
        LOG_WARNING => LogLevel::WARNING,
        LOG_ERR     => LogLevel::ERROR,
        LOG_CRIT    => LogLevel::CRITICAL,
        LOG_EMERG   => LogLevel::EMERGENCY,
    ];

    /** @var EventDispatcherInterface */
    private $eventDispatcher;
    /** @var array */
    private $defaultRdKafkaOptions;
    /** @var LoggerInterface|null */
    private $logger;

    /**
     * @param EventDispatcherInterface $eventDispatcher
     * @param array $defaultRdKafkaOptions
     * @param LoggerInterface|null $logger
     */
    public function __construct(EventDispatcherInterface $eventDispatcher, array $defaultRdKafkaOptions = [], LoggerInterface $logger = null)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->defaultRdKafkaOptions = $defaultRdKafkaOptions;
        $this->logger = $logger;
    }

    /**
     * @param string $dsn
     * @param array $options
     * @param SerializerInterface $serializer
     * @return TransportInterface
     */
    public function createTransport(string $dsn, array $options, SerializerInterface $serializer): TransportInterface
    {
        $rdKafkaConfiguration = $this->createRdKafkaConfiguration($options['rdkafka'] ?? []);
        $configuration = $this->createKafkaTransportConfiguratuion($dsn, $options)
            ->setRdCafkaConf($rdKafkaConfiguration);

        return new KafkaTransport($serializer, $this->eventDispatcher, $configuration, $this->logger);
    }

    /**
     * @param string $dsn
     * @param array $options
     * @return bool
     */
    public function supports(string $dsn, array $options): bool
    {
        return 0 === strpos($dsn, static::PROTOCOL . '://');
    }

    /**
     * @param array $rdkafkaOptions
     * @return RdKafka\Conf
     */
    private function createRdKafkaConfiguration(array $rdkafkaOptions): RdKafka\Conf
    {
        $rdKafkaConfiguration = new RdKafka\Conf();
        $rdkafkaOptions = array_merge([
            'auto.offset.reset'        => 'end',
            'max.in.flight'            => 1,
            'enable.auto.commit'       => 0,
            'enable.auto.offset.store' => 0,
        ], $this->defaultRdKafkaOptions, $rdkafkaOptions);

        foreach ($rdkafkaOptions as $optionName => $value) {
            $rdKafkaConfiguration->set($optionName, $value);
        }

        if (null !== $this->logger) {
            $rdKafkaConfiguration->setLogCb(function ($kafka, $level, $facility, $message) {
                $this->logger->log(self::LOG_LEVEL_MAP[$level], sprintf('[%s] %s', $facility, $message));
            });
        }

        $rdKafkaConfiguration->setErrorCb(function ($kafka, $err, $reason) {
            $errMessage = sprintf('%s. Reason: %s', rd_kafka_err2str($err), $reason);
            if (RD_KAFKA_RESP_ERR__FATAL === $err) {
                throw new KafkaConsumeException($errMessage, $err);
            }

            if (null !== $this->logger) {
                $this->logger->error($errMessage);
            }
        });

        $rdKafkaConfiguration->setRebalanceCb(function (RdKafka\KafkaConsumer $kafka, int $err, array $partitions) {
            /** @var RdKafka\TopicPartition[] $partitions */
            switch ($err) {
                case RD_KAFKA_RESP_ERR__ASSIGN_PARTITIONS:
                    $kafka->assign($partitions);
                    if (null !== $this->logger) {
                        foreach ($partitions as $partition) {
                            $this->logger->debug(sprintf(
                                'Assigned topic %s; partition %s',
                                $partition->getTopic(),
                                $partition->getPartition()
                            ));
                        }
                    }
                    break;
                case RD_KAFKA_RESP_ERR__REVOKE_PARTITIONS:
                    $kafka->assign();
                    if (null !== $this->logger) {
                        $this->logger->debug('Partitions revoked');
                    }
                    break;
                default:
                    $kafka->assign();
                    throw new KafkaConsumeException(rd_kafka_err2str($err), $err);
            }
        });

        return $rdKafkaConfiguration;
    }

    /**
     * @param string $dsn
     * @param array $options
     * @return KafkaTransportConfiguration
     */
    private function createKafkaTransportConfiguratuion(string $dsn, array $options): KafkaTransportConfiguration
    {
        $params = parse_url($dsn);

        $cofiguration = (new KafkaTransportConfiguration())
            ->setHeaders($options['headers'] ?? [])
            ->setTopicName($params['host']);

        if (isset($options['read_timeout'])) {
            $cofiguration->setReadTimeoutMs($options['read_timeout']);
        }

        if (isset($options['write_timeout'])) {
            $cofiguration->setWriteTimeoutMs($options['write_timeout']);
        }

        return $cofiguration;
    }
}
