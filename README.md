# Symfony Kafka transport

Описание
--------

Клиент PHP для системы обмена сообщениями Apache Kafka.

Установка
---------

```shell
$ composer require coooller/symfony-messenger-kafka-transport
```

Документация
------------

- [Документация](https://symfony.com/doc/4.4/messenger.html) при работе с пакетом.
